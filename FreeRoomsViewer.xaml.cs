﻿using Hotel_Final.DataAccesLayer;
using Hotel_Final.DomainModel;
using Hotel_Final.ServiceLayer;
using Hotel_Final.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel_Final
{
    /// <summary>
    /// Interaction logic for FreeRoomsViewer.xaml
    /// </summary>
    public partial class FreeRoomsViewer : Window, INotifyPropertyChanged
    {
        private DateTime startDate;
        private DateTime endDate;
        private MTObservableCollection<Room> roomList = new MTObservableCollection<Room> ();
        private ApplicationContext appContext = ApplicationContext.Instance;

        private Room selectedRoom;


        public event PropertyChangedEventHandler PropertyChanged;

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }

            set
            {
                startDate = value;
                appContext.GetRoomList (SetRoomList, startDate, endDate);
                NotifyPropertyChanged ();
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }

            set
            {
                endDate = value;
                appContext.GetRoomList (SetRoomList, startDate, endDate);
                NotifyPropertyChanged ();
            }
        }

        public MTObservableCollection<Room> RoomList
        {
            get
            {
                return roomList;
            }

            set
            {
                roomList = value;
            }
        }

        public Room SelectedRoom
        {
            get
            {
                return selectedRoom;
            }

            set
            {
                selectedRoom = value;
                NotifyPropertyChanged ();//las-o aici ca nici eu nu stiu, sterge comentariul
            }
        }

        private void SetRoomList (List<Room> rl)
        {
            if (rl != null)
            {
                roomList.Clear ();
                foreach (var r in rl)
                {
                    roomList.Add (r);
                }
            }
        }

        private void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        public FreeRoomsViewer ()
        {
            InitializeComponent ();

            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }

        private void btnBook_Click (object sender, RoutedEventArgs e)
        {
            if(appContext.Account as Client != null)
            {
                Console.WriteLine ("Gigele vezi ca a intrat un client!");
                Booking bk = new Booking ();
                bk.Client = appContext.Account as Client;
                bk.CheckIn = StartDate;
                bk.CheckOut = EndDate;
                bk.Room = SelectedRoom;
                bk.TotalCost = 1;//lasa-ma ca-s generos

                HotelService.addBooking (bk);
                StartDate = StartDate;
            }
        }
    }
}
