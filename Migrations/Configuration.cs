namespace Hotel_Final.Migrations
{
    using DomainModel;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Hotel_Final.DataAccesLayer.HotelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Hotel_Final.DataAccesLayer.HotelContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Accounts.AddOrUpdate (a => a.Username,
                new Client { Username = "ion", Money = 1000, Name = "Ion", Password = "1234"},
                new Administrator { Username = "alec", Name = "Alec", Password = "1234" }
                );
            context.Rooms.AddOrUpdate (r => r.RoomNumber,
                new Room { RoomNumber = "315", CostPerNight = 25, RoomType = RoomType.Quad}
                );
        }
    }
}
