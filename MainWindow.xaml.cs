﻿using Hotel_Final.DataAccesLayer;
using Hotel_Final.DomainModel;
using Hotel_Final.ServiceLayer;
using Hotel_Final.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hotel_Final
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private MTObservableCollection<Room> roomList = new MTObservableCollection<Room> ();
        private ApplicationContext appContext = ApplicationContext.Instance;
        private Room selectedRoom;

        public event PropertyChangedEventHandler PropertyChanged;

        public Room SelectedRoom
        {
            get { return selectedRoom; }
            set
            {
                selectedRoom = value;
                NotifyPropertyChanged ();
            }
        }

        public ApplicationContext AppContext
        {
            get
            {
                return appContext;
            }
        }

        public MTObservableCollection<Room> RoomList
        {
            get
            {
                return roomList;
            }
        }

        private void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }


        public MainWindow ()
        {
            InitializeComponent ();

            appContext.GetRoomList (SetRoomList);
        }

        private void SetRoomList (List<Room> rl)
        {
            if (rl != null)
            {
                roomList.Clear ();
                foreach (var r in rl)
                {
                    roomList.Add (r);
                }
            }
        }


        private void btnLogout_Click (object sender, RoutedEventArgs e)
        {
            appContext.Account = null;
        }

        private void btnLogin_Click (object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow ();
            lw.ShowDialog ();
        }

        private void btnRegister_Click (object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow (true);
            lw.ShowDialog ();
        }

        private void btnUpdate_Click (object sender, RoutedEventArgs e)
        {
            HotelService.updateCurrentAccount ();
        }

        private void btnDelete_Click (object sender, RoutedEventArgs e)
        {
            var rez = MessageBox.Show ("Be ye sure ye want to delete th' account?", "Warnin'!", MessageBoxButton.YesNo);
            if (rez == MessageBoxResult.Yes)
            {
                if (HotelService.deleteAccount (appContext.Account))
                {
                    appContext.Account = null;
                    MessageBox.Show ("Jolly b!");
                }
            }
        }

        private void btnAddRoom_Click (object sender, RoutedEventArgs e)
        {
            if (appContext.Account as Administrator != null)
            {
                AddNewRoom anr = new AddNewRoom ();
                anr.ShowDialog ();
                appContext.GetRoomList (SetRoomList);
            }

        }

        private void btnUpdateRoom_Click (object sender, RoutedEventArgs e)
        {
            if (appContext.Account as Administrator != null && SelectedRoom != null)
            {
                AddNewRoom anr = new AddNewRoom (SelectedRoom);
                anr.ShowDialog ();
                appContext.GetRoomList (SetRoomList);
            }
        }

        private void btnDeleteRoom_Click (object sender, RoutedEventArgs e)
        {
            if (appContext.Account as Administrator == null)
                return;

            if (SelectedRoom != null)
            {
                if (HotelService.deleteRoom (SelectedRoom))
                {
                    appContext.GetRoomList (SetRoomList);
                    SelectedRoom = null;
                }
                else
                {
                    MessageBox.Show ("Ye like t' play death!");
                }
            }
            else
            {
                MessageBox.Show ("Select a cabin!");
            }
        }

        private void btnViewRooms_Click (object sender, RoutedEventArgs e)
        {
            RoomCatalogWindow rcw = new RoomCatalogWindow ();
            rcw.ShowDialog ();
        }

        private void btnAddEmployee_Click (object sender, RoutedEventArgs e)
        {
            if (appContext.Account as Administrator != null)
            {
                LoginWindow lw = new LoginWindow (true, 2);
                lw.ShowDialog ();
            }
        }

        private void btnViewFreeRooms_Click (object sender, RoutedEventArgs e)
        {
            FreeRoomsViewer frv = new FreeRoomsViewer ();
            frv.ShowDialog ();
        }
    }
}
