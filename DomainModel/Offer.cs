﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Offer
    {
        private int id;
        private int priceCutPercentage;
        private List<RoomType> affectedRoomTypes;
        private DateTime startPromotionDate;
        private DateTime endPromotionDate;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public int PriceCutPercentage
        {
            get
            {
                return priceCutPercentage;
            }

            set
            {
                priceCutPercentage = value;
            }
        }

        public List<RoomType> AffectedRoomTypes
        {
            get
            {
                return affectedRoomTypes;
            }

            set
            {
                affectedRoomTypes = value;
            }
        }

        public DateTime StartPromotionDate
        {
            get
            {
                return startPromotionDate;
            }

            set
            {
                startPromotionDate = value;
            }
        }

        public DateTime EndPromotionDate
        {
            get
            {
                return endPromotionDate;
            }

            set
            {
                endPromotionDate = value;
            }
        }
    }
}
