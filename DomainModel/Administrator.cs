﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Administrator : Account
    {
        [NotMapped]
        public override string AccountType
        {
            get
            {
                return "Administrator";
            }
        }

        [NotMapped]
        public override bool IsValid
        {
            get {
                if (Username == null || Username.Length < 4 || Username.Length > 30)
                    return false;
                if (Password == null || Password.Length < 4 || Password.Length > 30)
                    return false;
                if (Name == null || Name.Length < 4 || Name.Length > 30)
                    return false;
                return true;
            }
        }
        public override string ToString ()
        {
            return string.Format("{0}", base.ToString ());
        }
    }
}
