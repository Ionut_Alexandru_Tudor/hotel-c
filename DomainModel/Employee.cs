﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Employee : Account
    {
        [NotMapped]
        public override string AccountType
        {
            get
            {
                return "Employee";
            }
        }
    }
}
