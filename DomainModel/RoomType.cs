﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public enum RoomType
    {
        Single,
        Double,
        Matrimonial,
        Triple,
        Quad
    }
}
