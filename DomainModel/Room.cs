﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Room : INotifyPropertyChanged
    {
        private int id;

        private string roomNumber;

        private int costPerNight;

        private RoomType roomType;

        private bool isReserved = false;

        private bool hasTV = false;

        private bool hasBathRoom = false;

        private bool hasInternet = false;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        [Index (IsUnique = true)]
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
                NotifyPropertyChanged ();
            }
        }

        [Index (IsUnique = true)]
        [StringLength (30)]
        public string RoomNumber
        {
            get
            {
                return roomNumber;
            }

            set
            {
                roomNumber = value;
                NotifyPropertyChanged ();
            }
        }

        public int CostPerNight
        {
            get
            {
                return costPerNight;
            }

            set
            {
                costPerNight = value;
                NotifyPropertyChanged ();
            }
        }

        public RoomType RoomType
        {
            get
            {
                return roomType;
            }

            set
            {
                roomType = value;
                NotifyPropertyChanged ();
            }
        }

        public bool IsReserved
        {
            get
            {
                return isReserved;
            }

            set
            {
                isReserved = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasTV
        {
            get
            {
                return hasTV;
            }

            set
            {
                hasTV = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasBathRoom
        {
            get
            {
                return hasBathRoom;
            }

            set
            {
                hasBathRoom = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasInternet
        {
            get
            {
                return hasInternet;
            }

            set
            {
                hasInternet = value;
                NotifyPropertyChanged ();

            }
        }

        public Room clone ()
        {
            Room r = new Room ();
            r.costPerNight = costPerNight;
            r.hasBathRoom = hasBathRoom;
            r.hasInternet = hasInternet;
            r.hasTV = hasTV;
            r.id = id;
            r.isReserved = isReserved;
            r.roomNumber = roomNumber;
            r.roomType = roomType;

            return r;
        }

        public override string ToString ()
        {
            return string.Format ("id = {0}|num = {1}|cpn = {2}|rt = {3}|isr = {4}", id, roomNumber, costPerNight, roomType, isReserved);
        }
    }
}
