﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Booking
    {
        private int id;
        private Room room;
        private DateTime checkIn;
        private DateTime checkOut;
        private int totalCost;
        private bool payed;
        private bool canceled;
        private Client client;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public Room Room
        {
            get
            {
                return room;
            }

            set
            {
                room = value;
            }
        }

        public DateTime CheckIn
        {
            get
            {
                return checkIn;
            }

            set
            {
                checkIn = value;
            }
        }

        public DateTime CheckOut
        {
            get
            {
                return checkOut;
            }

            set
            {
                checkOut = value;
            }
        }

        public int TotalCost
        {
            get
            {
                return totalCost;
            }

            set
            {
                totalCost = value;
            }
        }

        public bool Payed
        {
            get
            {
                return payed;
            }

            set
            {
                payed = value;
            }
        }

        public bool Canceled
        {
            get
            {
                return canceled;
            }

            set
            {
                canceled = value;
            }
        }

        public Client Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
            }
        }
    }
}
