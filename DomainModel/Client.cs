﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
 using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public class Client : Account
    {
        private int money;

        [NotMapped]
        public override string AccountType
        {
            get
            {
                return "Client";
            }
        }

        public int Money
        {
            get
            {
                return money;
            }

            set
            {
                money = value;
                NotifyPropertyChanged ();
            }
        }

        public override string ToString ()
        {
            return string.Format ("{0}|money = {1}", base.ToString (), money);
        }
    }
}
