﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DomainModel
{
    public abstract class Account : INotifyPropertyChanged
    {
        private int id;

        private string username;

        private string password;


        private string name;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
                NotifyPropertyChanged ();
            }
        }

        [StringLength (30)]
        [Index (IsUnique = true)]
        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
                NotifyPropertyChanged ();
            }
        }

        [StringLength (30)]
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
                NotifyPropertyChanged ();
            }
        }

        [StringLength (30)]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                NotifyPropertyChanged ();
            }
        }

        [NotMapped]
        public virtual bool IsValid
        {
            get
            {
                if (username == null || username.Length < 5 || username.Length > 30)
                    return false;
                if (password == null || password.Length < 5 || password.Length > 30)
                    return false;
                if (name == null || name.Length < 5 || name.Length > 30)
                    return false;
                return true;
            }
        }

        [NotMapped]
        public abstract string AccountType
        {
            get;
        }

        public override string ToString ()
        {
            return string.Format ("id = {0}|username = {1}|password = {2}|name = {3}", id, username, password, name);
        }
    }
}
