﻿using Hotel_Final.DomainModel;
using Hotel_Final.ServiceLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Hotel_Final.DataAccesLayer
{
    public class ApplicationContext : INotifyPropertyChanged
    {
        public delegate void GetRoomListCallBack (List<Room> rooms);

        private Account account;

        public event PropertyChangedEventHandler PropertyChanged;

        private static ApplicationContext instance = new ApplicationContext ();
        public static ApplicationContext Instance { get { return instance; } }

        public Account Account
        {
            get { return account; }
            set
            {
                account = value;
                NotifyPropertyChanged ();
                NotifyPropertyChanged ("GuestVisibility");
                NotifyPropertyChanged ("ClientVisibility");
                NotifyPropertyChanged ("AdministratorVisibility");
                NotifyPropertyChanged ("EmployeeVisibility");
                NotifyPropertyChanged ("AccountVisibility");
            }
        }

        public void GetRoomList (Action<List<Room>> setRoomList, DateTime startDate, DateTime endDate)
        {
            if(startDate > endDate)
            {
                Console.WriteLine ("EROARE! {0} > {1}", startDate, endDate);
                return;
            }
            Thread th = new Thread (() =>
            {
                var list = HotelService.getAvailableRooms (startDate, endDate);
                setRoomList (list);
            });
            th.Start ();
        }

        public Visibility AccountVisibility
        {
            get
            {
                return Account != null ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility GuestVisibility
        {
            get
            {
                return Account == null ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility ClientVisibility
        {
            get
            {
                return Account as Client != null ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility AdministratorVisibility
        {
            get
            {
                return Account as Administrator != null ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility EmployeeVisibility
        {
            get
            {
                return Account as Employee != null ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private ApplicationContext ()
        {
        }

        private  void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        public void GetRoomList (GetRoomListCallBack grlcb)
        {
            Thread th = new Thread (() =>
            {
                var list = HotelService.getAllRooms ();
                grlcb (list);
            });
            th.Start ();
        }
    }
}
