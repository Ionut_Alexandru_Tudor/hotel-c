﻿using Hotel_Final.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.DataAccesLayer
{
    public class HotelContext : DbContext
    {

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Room> Rooms { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public HotelContext () : base ("myConnectionString")
        {
            Debug.WriteLine (Database.Connection.ConnectionString);
        }
    }
}
