﻿using Hotel_Final.DataAccesLayer;
using Hotel_Final.DomainModel;
using Hotel_Final.ServiceLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel_Final
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window, INotifyPropertyChanged
    {
        private ApplicationContext appContext = ApplicationContext.Instance;

        public event PropertyChangedEventHandler PropertyChanged;

        private string username = "alec";
        private string password = "1234";
        private string cName = "Alec";
        private int money = 1000;
        private bool register;
        private int accountType;

        public Visibility RegisterVisibility
        {
            get
            {
                return Register ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public ApplicationContext AppContext { get { return appContext; } }

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
                NotifyPropertyChanged ();
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                NotifyPropertyChanged ();
            }
        }

        public bool Register
        {
            get
            {
                return register;
            }

            set
            {
                register = value;
                NotifyPropertyChanged ();
                NotifyPropertyChanged ("RegisterVisibility");
            }
        }


        public int Money
        {
            get
            {
                return money;
            }

            set
            {
                money = value;
                NotifyPropertyChanged ();
            }
        }

        public string CName
        {
            get
            {
                return cName;
            }

            set
            {
                cName = value;
                NotifyPropertyChanged ();
            }
        }

        public LoginWindow ()
        {
            InitializeComponent ();
        }

        public LoginWindow (bool register) : this ()
        {
            Register = register;
            accountType = 1;
        }

        public LoginWindow (bool register, int accountType = 1) : this (register)
        {
            this.accountType = accountType;
        }

        private void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        private void btnLogin_Click (object sender, RoutedEventArgs e)
        {
            var acc = HotelService.getAccount (Username, Password);
            if (acc != null)
            {
                appContext.Account = acc;
                Close ();
            }
            else
            {
                MessageBox.Show ("Incorrect piratename or passcode!", "Oy!");
            }
        }

        private void btnRegister_Click (object sender, RoutedEventArgs e)
        {
            Account a;
            switch (accountType)
            {
                case 1:
                    a = new Client ();
                    (a as Client).Money = Money;
                    break;
                case 2:
                    a = new Employee ();
                    break;
                case 3:
                    a = new Administrator ();
                    break;
                default:
                    return;
            }
            a.Username = Username;
            a.Password = Password;
            a.Name = CName;

            var acc = HotelService.addAccount (a);
            if (acc != null)
            {
                appContext.Account = acc;
                Close ();
            }
            else
            {
                MessageBox.Show ("Give a go' again!\n" +
                                    "1)Piratename, passcode 'n moniker must be longer than 5!\n" +
                                    "2)Chose another piratename!\n");
            }
        }
    }
}
