﻿using Hotel_Final.DataAccesLayer;
using Hotel_Final.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.ServiceLayer
{
    public static class HotelService
    {
        private static object dbLock = new object ();
        #region create
        public static Account addAccount (Account account)
        {
            lock (dbLock)
            {
                if (account.IsValid == false)
                {
                    return null;
                }
                try
                {
                    using (var context = new HotelContext ())
                    {
                        context.Accounts.Add (account);
                        context.SaveChanges ();
                        return getAccount(account.Username, account.Password);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine (e.Message);
                    return null;
                }
            }
        }
        public static Room addRoom (Room room)
        {
            lock (dbLock)
            {
                try
                {
                    using (var context = new HotelContext ())
                    {
                        context.Rooms.Add (room);
                        context.SaveChanges ();
                        return room;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine (e.Message);
                    return null;
                }
            }
        }

        public static Booking addBooking (Booking book)
        {
            lock (dbLock)
            {
                try
                {
                    using (var context = new HotelContext ())
                    {
                        context.Bookings.Add (book);
                        context.SaveChanges ();
                        return book;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine (e.Message);
                    return null;
                }
            }
        }
        #endregion

        #region read
        public static Account getAccount(string username, string password)
        {
            lock (dbLock)
            {
                if (username == null || password == null)
                    return null;
                using (var context = new HotelContext ())
                {
                    return context.Accounts.FirstOrDefault (a => a.Username.Equals (username) && a.Password.Equals (password));
                }
            }
        }
        public static List<Room> getAvailableRooms(DateTime start, DateTime end)
        {
            lock (dbLock)
            {
                using (var context = new HotelContext ())
                {
                    var bList = context.Bookings.Where (b => b.CheckOut >= start || b.CheckIn <= end).Select(r => r.Room);
                    var rList = context.Rooms.Where (r => bList.Contains (r) == false);
                    return rList.ToList ();
                }
            }
        }

        public static Room getRoom(Room room)
        {
            lock (dbLock)
            {
                using (var context = new HotelContext ())
                {
                    return context.Rooms.FirstOrDefault (b => b.RoomNumber.Equals (room.RoomNumber));
                }
            }
        }
        public static List<Room> getAllRooms ()
        {
            lock (dbLock)
            {
                using (var context = new HotelContext ())
                {
                    return context.Rooms.ToList ();
                }
                
            }
        }


        #endregion

        #region update
        public static bool updateCurrentAccount ()
        {
            lock (dbLock)
            {
                var acc = ApplicationContext.Instance.Account;
                if (acc != null)
                {
                    try
                    {
                        using (var context = new HotelContext ())
                        {
                            context.Accounts.Attach (acc);
                            context.Entry (acc).State = EntityState.Modified;
                            context.SaveChanges ();
                            return true;
                        }
                    }
                    catch(Exception e)
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        public static bool updateRoom (Room room)
        {
            lock (dbLock)
            {
                if (room != null)
                {
                    try
                    {
                        using (var context = new HotelContext ())
                        {
                            context.Rooms.Attach (room);
                            context.Entry (room).State = EntityState.Modified;
                            context.SaveChanges ();
                            return true;
                        }
                    }
                    catch(Exception e)
                    {
                    }
                }
                return false;
            }
        }
        #endregion

        #region delete
        public static bool deleteAccount (Account account)
        {
            lock (dbLock)
            {
                if (account != null)
                {
                    try
                    {
                        using (var context = new HotelContext ())
                        {
                            var rez = context.Accounts.FirstOrDefault (a => a.Id == account.Id);
                            context.Accounts.Remove (rez);
                            context.SaveChanges ();
                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        public static bool deleteRoom (Room selectedRoom)
        {
            lock (dbLock)
            {
                if(selectedRoom != null)
                {
                    try
                    {
                        using (var context = new HotelContext ())
                        {
                            var rez = context.Rooms.FirstOrDefault (r => r.Id == selectedRoom.Id);
                            context.Rooms.Remove (rez);
                            context.SaveChanges ();
                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        #endregion
    }
}
