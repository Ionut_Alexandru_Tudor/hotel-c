﻿using Hotel_Final.DomainModel;
using Hotel_Final.ServiceLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hotel_Final
{
    /// <summary>
    /// Interaction logic for AddNewRoom.xaml
    /// </summary>
    public partial class AddNewRoom : Window, INotifyPropertyChanged
    {
        private string roomNumber = "315";
        private int costPerNight = 25;
        private RoomType roomType = RoomType.Quad;
        private bool hasTV = false;
        private bool hasBathRoom = false;
        private bool hasInternet = false;

        private Room selectedRoom;

        public event PropertyChangedEventHandler PropertyChanged;

        public Room SelectedRoom
        {
            get { return selectedRoom; }
            set
            {
                selectedRoom = value;

                RoomNumber = selectedRoom.RoomNumber;
                CostPerNight = selectedRoom.CostPerNight;
                RoomType = selectedRoom.RoomType;
                HasTV = selectedRoom.HasTV;
                HasBathRoom = selectedRoom.HasBathRoom;
                HasInternet = selectedRoom.HasInternet;

                NotifyPropertyChanged ();
                NotifyPropertyChanged ("EditRoomVisibility");
                NotifyPropertyChanged ("CreateRoomVisibility");
            }
        }
        public string RoomNumber
        {
            get
            {
                return roomNumber;
            }
            set
            {
                roomNumber = value;
                NotifyPropertyChanged ();
            }
        }

        public int CostPerNight
        {
            get
            {
                return costPerNight;
            }

            set
            {
                costPerNight = value;
                NotifyPropertyChanged ();
            }
        }

        public RoomType RoomType
        {
            get
            {
                return roomType;
            }

            set
            {
                roomType = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasTV
        {
            get
            {
                return hasTV;
            }

            set
            {
                hasTV = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasBathRoom
        {
            get
            {
                return hasBathRoom;
            }

            set
            {
                hasBathRoom = value;
                NotifyPropertyChanged ();
            }
        }

        public bool HasInternet
        {
            get
            {
                return hasInternet;
            }

            set
            {
                hasInternet = value;
                NotifyPropertyChanged ();
            }
        }

        public Visibility EditRoomVisibility
        {
            get
            {
                Console.WriteLine ("Edit {0}", selectedRoom != null);
                return selectedRoom != null ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility CreateRoomVisibility
        {
            get
            {
                Console.WriteLine ("Create {0}", selectedRoom == null);
                return selectedRoom == null ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public AddNewRoom ()
        {
            InitializeComponent ();
        }

        public AddNewRoom (Room selectedRoom) : this ()
        {
            SelectedRoom = selectedRoom;
        }

        private void NotifyPropertyChanged ([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        private void btnCreateRoom_Click (object sender, RoutedEventArgs e)
        {
            if (selectedRoom != null)
                return;

            Room room = new Room ();
            room.RoomNumber = RoomNumber;
            room.CostPerNight = CostPerNight;
            room.HasBathRoom = HasBathRoom;
            room.HasInternet = HasInternet;
            room.HasTV = HasTV;
            room.IsReserved = false;
            room.RoomType = RoomType;

            room = HotelService.addRoom (room);
            if (room == null)
            {
                MessageBox.Show ("Cabin number already exists!", "Arrrrr!");
            }
            else
            {
                Close ();
            }
        }

        private void btnUpdateRoom_Click (object sender, RoutedEventArgs e)
        {
            var r = SelectedRoom.clone ();
            r.CostPerNight = CostPerNight;
            r.HasBathRoom = HasBathRoom;
            r.HasInternet = HasInternet;
            r.HasTV = HasTV;
            r.RoomNumber = RoomNumber;
            r.RoomType = RoomType;

            if(HotelService.updateRoom (r))
            {
                MessageBox.Show ("Cabin updated!");
                Close ();
            }
            else
            {
                MessageBox.Show ("Ye need to check th' cabin number!", "Arrrrr!");
            }
        }
    }
}
