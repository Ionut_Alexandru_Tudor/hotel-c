﻿using Hotel_Final.DataAccesLayer;
using Hotel_Final.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel_Final.ServiceLayer
{
    public class HotelService
    {
        public static bool addAccount(Account account)
        {
            using(var context = new HotelContext ())
            {
                context.Accounts.Add (account);
                context.SaveChanges ();
                return true;
            }
        }
    }
}
